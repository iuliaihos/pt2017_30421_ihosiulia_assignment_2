package client;

import java.util.Random;

public class Client {
   private String name;
   private int arrivalTime;
   private int serviceTime;
   private int exitTime;
   
   // the constructor computes a random in a certain range for the service time
  public Client(int serviceMin, int serviceMax)
   {
	   Random rand = new Random();
	   this.serviceTime = rand.nextInt(serviceMax - serviceMin + 1) + serviceMin; 
	   this.name = generateRandomClientName();
   }
   
  //used in the constructor for the client's name
   String generateRandomClientName()
   {
	   Random rand = new Random();
	   String letter = "aaaaabcdeeeeeefghiiiiiiijklmnooooopqrstuuuuuvwxyz";
	   int length = rand.nextInt(5) + 3;
	   String name = "";
	   for (int i = 0; i<length; i++)
		   name += letter.charAt(rand.nextInt(26));
	   return name;
   }
   
   public int getArrivalTime() {
		return arrivalTime;
	}
   
   public int getServiceTime() {
		return serviceTime;
	}
   
   public int getLeavingTime() {
		return exitTime;
	}
   
   public void setLeavingTime(int c) {
		 exitTime = c ;
	}
   
   
   public String getName()
   {
	   return name;
   }
   
   public void setArrivalTime(int currentTime) {
		arrivalTime = currentTime;
	}
   
   public boolean checkIfFinished()
   {
	   if (exitTime > 0) //exitTime > 0 means the client has already been served
		   return true;
	   else return false;
   }
   
   public String toString(){
		if(exitTime == 0)
			return "new arrival: "+ arrivalTime + ". " + name + " Service Time: " + String.valueOf(serviceTime) + "\n";
		else
			return name + ": arrived: "+ arrivalTime + "; service Time: " + String.valueOf(serviceTime) + ". Left at: "+ exitTime+"\n";
	
	}


}

