package thread;

import  client.Client;
import controller.Controller;

import java.util.ArrayList;
import java.util.List;

public class Server implements Runnable{

	private List <Client> queue;
	private Client first;
	private int totalWaitingTime;
	private int currentWaitingTime;
	private int totalNumberOfClients;
    private int extraTime;
	private double avgWaitingTime;
	private boolean closed;
	private boolean running;
	private Object lock = new Object();
	private String clientExitMessage;
	private boolean noMoreClientsToBeAdded;
	public String log;
	
	public Server()
	{
		queue = new ArrayList<Client>();
		totalWaitingTime = 0;
		totalNumberOfClients = 0;
		avgWaitingTime = 0;
		closed = false;
		first = null;
		running = true;
		log = "";
		noMoreClientsToBeAdded= false;
	}
	
	public void serve()
	{synchronized(lock)
		{
		if (queue.isEmpty())          //if queue is empty
		{if(noMoreClientsToBeAdded)  //  and no more clients are to be assigned (all have been assigned already)
			  terminate();}          //the thread is terminated
	else
    {
	int a =0; // the exit time for each client
	totalNumberOfClients ++;
	first.setArrivalTime(Controller.getCurrentTime());
	if (extraTime <= first.getArrivalTime()) // if the queue was empty when the client arrived
	      a = first.getArrivalTime()+ first.getServiceTime();  // only need to wait service time milliseconds
	else 
		  a = extraTime + first.getServiceTime(); //needs to wait extra time until he gets at the front
	int wait  = a - first.getArrivalTime(); //compute waiting time
	totalWaitingTime += wait;
	try {
		Thread.sleep(wait*1000);
	    } catch (InterruptedException e) {
		System.out.println("Interrupt detected"); }
	first.setLeavingTime(a);// set leaving time
	clientExitMessage = first + "waited for: " + wait + "\n"; 
	log=log+"\n"+first + "waited for: " + wait;
	extraTime = first.getLeavingTime();  //the exit time of each leaving client is kept 
	if (!queue.isEmpty())
	   if (queue.get(0).checkIfFinished())
		  queue.remove(queue.get(0));  //remove the sereved customer
	if (!queue.isEmpty()) first = queue.get(0); // prepare to serve the next one.
}}}
	
	public void run()
	{
		
		while (running) {
			serve();
		}
		
	}
	
	public String  getExitMessage()
	{
		return clientExitMessage;
	}
	
	// computes the average waiting time for each queue
	public void computeAvgWaitingTime()
	{
		if (totalNumberOfClients != 0)
			avgWaitingTime = totalWaitingTime/(double)totalNumberOfClients;
	}
	
	public double getAvgWaitingTime()
	{
		computeAvgWaitingTime();
		return avgWaitingTime;
	}
	
	//answers whether the queue is closed 
	public boolean getState()
	{
		return closed;
	}
	
	public int getNumberOfClients()
	{
		return totalNumberOfClients;
	}
	
	public int getWaitingTime()
	{
		return totalWaitingTime;
	}
	
	public int getCurrentWaitingTime()
	{
		int i =0;
		for(Client c: queue)
		{
			i += c.getServiceTime();
		}
		currentWaitingTime = i;
		return currentWaitingTime;
	}
	
	public void stopWhenEmpty()
	{
	  setNoMoreClientsToBeAdded(true);
	}
	
	public int getCurrentNumberOfClients()
	{
		return queue.size();
	}
	
	// adds clients to the list of clients 
	public void addClient(Client c)
	{
		if (queue.isEmpty())
			first = c;
		queue.add(c);
		System.out.println("added " + c.getName());
	}
	
	void removeClient(Client c)
	{
		queue.remove(c);
	}
	
	
	public void terminate()
	{
		closed = true;
		this.running = false;
	}
	
	public String toString()
	{
		return log;
	}

	public void setNoMoreClientsToBeAdded(boolean noMoreClientsToBeAdded) {
		this.noMoreClientsToBeAdded = noMoreClientsToBeAdded;
	}

}
