package thread;

import client.Client;

public class ThreadStarter {
	private  Server server;
	private Thread thread;
	
	
	
	public ThreadStarter(){
		setServer(new Server());              //creates new server object
		setThread(new Thread(getServer()));   // and uses it to create a thread object
		getThread().start();
		
		
	}
	
	//ads client to server
	public void add(Client c)
	{
	getServer().addClient(c);
	}
	
	public void remove(Client c)
	{
	getServer().removeClient(c);
	}
	
	//sends termination signal to server
	public void stop()
	{
		getServer().terminate();
	}
	
	//signals server to stop when empty
	public void stopWhenEmpty()
	{
	   getServer().setNoMoreClientsToBeAdded(true);
	}
	
	
	public double getAvg()
	{
		return getServer().getAvgWaitingTime();
	}
	

	public int getNoClients()
	{
		return getServer().getNumberOfClients();
	}
	
	public int getWaitingTime()
	{
		return getServer().getWaitingTime();
	}
	
	public int getCurrentNoOfClients()
	{
		return getServer().getCurrentNumberOfClients();
	}
	
	public Thread getThread() {
		return thread;
	}
	
	public void setThread(Thread thread) {
		this.thread = thread;
	}
	
	public Server getServer() {
		return server;
	}
	
	public void setServer(Server server) {
		this.server = server;
	}

}
