package controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import client.Client;
import frame.Frame;
import thread.ThreadStarter;

public class Controller implements Runnable{
	
	private int maxNumberQueues;
	private int simulationTime;
	private int minArrivalTime;
	private int maxArrivalTime;
	private int minServiceTime;
	private int maxServiceTime;
	private int maxNumberOfClients;
	private int peakTime;
	private static int currentTime;
	private Object lock = new Object();
	private String log ;
	private String logQueues ;
	private boolean running;
	private List<Client> clients ;
	private ThreadStarter  [] threads;
	
	
	
	public Controller(int maxQueues, int total, int minArrival, int maxArrival, int minService, int maxService)
	{   
		maxNumberQueues = maxQueues;
		simulationTime = total;
	    minArrivalTime = minArrival;
		maxArrivalTime = maxArrival;
		minServiceTime = minService;
		maxServiceTime = maxService;
		clients = new ArrayList<Client>();
		threads= new ThreadStarter[maxQueues];
		currentTime = 0;
		log = "";
		running = true;
		Random rand = new Random();
		initialise(rand.nextInt(30));
		
	}
	
	//creates all the clients and the queues.
	void initialise(int n)
	{
		//creates and adds clients in a list
		for (int i = 0; i < n; i++)
		{
			Client c = new Client(minServiceTime,maxServiceTime);
			clients.add(c);
			
		}
		System.out.println(n+" clients generated");
		log += n+" clients generated "+ "<br>" ; // to be displayed on the gui
		
		//create maxNumberQueues threads
		for (int i=0;i<maxNumberQueues;i++)
		{
			threads[i] = new ThreadStarter();
			
			
		}
		System.out.println(" created "+ maxNumberQueues + " threads");
		log += " created "+ maxNumberQueues + " threads <br>";
	}
	
	//adds clients in queues 
	public void manage()
	{synchronized(lock)
		{ 
		        Random rand = new Random();
				computePeak();  //each step the peak is computed
				System.out.println(getCurrentTime());
			    int n = rand.nextInt(maxArrivalTime - minArrivalTime + 1) + minArrivalTime;
				int enqueue = rand.nextInt(maxNumberQueues); //chooses in which queue to add the client
				try {
						Thread.sleep(n*1000);
					} catch (InterruptedException e) {
						System.out.println("Interrupt detected");
					} //the client can now be added
				setCurrentTime(getCurrentTime() + n);
				if(clients.size()>0) 
					{threads[enqueue].add(clients.get(0));
                    log += clients.get(0).getName()+ " arrived at "+ currentTime + " and was added to queue " + enqueue +"<br>";
					}
				Frame.updateCounter(currentTime);  //update the displays on the gui
				Frame.updateLog(log);
				Frame.updateQueues(toString());
				if(clients.size()>0)clients.remove(clients.get(0));
				if(clients.size() == 0)
					for(int i =0;i<maxNumberQueues;i++)
					      threads[i].stopWhenEmpty();
			 checkSimulationStatus();
			
		}
	}
	
	//prints the statistics in the console
	public void showLogs()
	{
		 System.out.println("----------------------------------------\n");
		for (int i=0;i<maxNumberQueues;i++)
		{   
			System.out.print(i + ": the average waiting time is " + threads[i].getAvg() + ", "+ threads[i].getNoClients()+ " clients served and the total waiting time was "+ threads[i].getWaitingTime());
			System.out.println(threads[i].getServer().log);
			System.out.println();
		}
		computePeak();
		System.out.println("the peak time was at: " + peakTime + " with " + maxNumberOfClients + " clients");
		
	}
	
	public void run()
	{

		while(running)
		  manage();
		
	}
	
	//checks if the simulationTime has been passed or if all the queues are closed
	//if either is true, the method terminates the current thread and also the threads for each queue
	
	public void checkSimulationStatus()
	{
		boolean done = true;
		for (int i=0;i<maxNumberQueues;i++)
			if(!threads[i].getServer().getState())
			{
				done = false;
			}
		if (currentTime >= simulationTime || done)
			{
			terminate();
			closeQueues();
	
			}
		
	}
	
	public  void terminate()
	{
		
		showLogs();
		Frame.updateLog(log + "<br>Simulation is over");
		running = false;
	}
	
	//stop the threads 
	void closeQueues()
	{
		for (int i=0;i<maxNumberQueues;i++)
		{
			threads[i].stop();
		}
	}
	
	public void computePeak()
	{
		int temp = 0;
		for (int i=0;i<maxNumberQueues;i++)
		{
			temp += threads[i].getCurrentNoOfClients();  //adds the number of clients in all queues
			
		}
		if (temp > maxNumberOfClients)   // a new peak has been reached
			     {
			     maxNumberOfClients = temp;
		         peakTime = getCurrentTime();  //save the peak time
		         }
		
	}
	
        public void setMaxNumberQueues(int c)
		{
			maxNumberQueues = c;
		}
		
		public void setSimulationTime(int c)
		{
			simulationTime = c;
		}
		
		public void setMinArrivalTime(int c)
		{
			minArrivalTime = c;
		}
		
		public void setMaxArrivalTime(int c)
		{
			maxArrivalTime = c;
		}
		
		public void setMaxServiceTime(int c)
		{
			minServiceTime = c;
		}
		
		public void setMinServiceTime(int c)
		{
			maxServiceTime = c;
		}

		public static void setCurrentTime(int currentTime) {
			Controller.currentTime = currentTime;
		}

		public static int getCurrentTime() {
			return currentTime;
		}
        
		public String toString()
		{
			logQueues = "";
			for(int i =0;i<maxNumberQueues;i++)
			if(threads[i].getServer().getExitMessage() != null)
				logQueues += threads[i].getServer().getExitMessage()+ "<br>";
			return logQueues;
		}
}
