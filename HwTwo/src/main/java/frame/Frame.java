package frame;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import controller.Controller;

public class Frame extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel pText = new JPanel();
	private JPanel pButon = new JPanel();
	private JTextField total = new JTextField();
	private JTextField minArrival = new JTextField();
	private JTextField maxArrival = new JTextField();
	private JTextField minService = new JTextField();
	private JTextField maxService = new JTextField();
	private JTextField maxQueues = new JTextField();
	private static JPanel text ;
	private static JPanel text1 ;
	private JButton start = new JButton("Start");
	private JButton stop = new JButton("Stop");
	private static JLabel counter = new JLabel("0");
	private Controller c;
	
	public Frame() {
       
		super("Simulator");
		setLayout(new FlowLayout());
		JPanel p1 = new JPanel();
		p1.add(new JLabel("Simulation Interval:"));
		total.setColumns(5);
		p1.add(total);
		pText.add(p1);
		
		JPanel p6 = new JPanel();
		p6.add(new JLabel("Max Queues:"));
		maxQueues.setColumns(5);
		p6.add(maxQueues);
		pText.add(p6);
		
		JPanel p2 = new JPanel();
		p2.add(new JLabel("Min Arrival Interval:"));
		minArrival.setColumns(5);
		p2.add(minArrival);
		pText.add(p2);

		JPanel p3 = new JPanel();
		p3.add(new JLabel("Max Arrival Interval:"));
		maxArrival.setColumns(5);
		p3.add(maxArrival);
		pText.add(p3);

		JPanel p4 = new JPanel();
		p4.add(new JLabel("Min Service Time:"));
		minService.setColumns(5);
		p4.add(minService);
		pText.add(p4);

		JPanel p5 = new JPanel();
		p5.add(new JLabel("Max Service Time:"));
		maxService.setColumns(5);
		p5.add(maxService);
		pText.add(p5);
		
		
		JPanel p7 = new JPanel();
		p7.add(start);
		p7.add(stop);
		pButon.add(p7);
		
		JPanel p8 = new JPanel();
		p8.add(counter);
		
		add(pText);
		add(pButon);
		add(p8);
		text = new JPanel();
		text1 = new JPanel();
		add(text);
		add(text1);
		
		start.addActionListener(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event) {
				if (Integer.parseInt(maxArrival.getText())-Integer.parseInt(minArrival.getText())<0)
					JOptionPane.showMessageDialog(null, "maximum time for arrival should be greater or equal to minimum time for arrival", "Input Error",
							JOptionPane.ERROR_MESSAGE);
				if (Integer.parseInt(maxService.getText())-Integer.parseInt(minService.getText())<0)
					JOptionPane.showMessageDialog(null, "maximum time needed for service should be greater or equal to minimum time for service", "Input Error",
							JOptionPane.ERROR_MESSAGE);
				if ( Integer.parseInt(total.getText())<0 ||Integer.parseInt( maxService.getText())< 0 || Integer.parseInt(minService.getText())<0
						|| Integer.parseInt(minArrival.getText())<0 || Integer.parseInt(maxArrival.getText())<0 ||
						Integer.parseInt(maxQueues.getText())<0)
					JOptionPane.showMessageDialog(null, "all values should be greater than zero", "Input Error",
							JOptionPane.ERROR_MESSAGE);
					
				c = new Controller(Integer.parseInt(maxQueues.getText()),
						                      Integer.parseInt(total.getText()),              
						                      Integer.parseInt(minArrival.getText()),
						                      Integer.parseInt(maxArrival.getText()),
						                      Integer.parseInt(minService.getText()),
						                      Integer.parseInt(maxService.getText()));	
				Thread t = new Thread(c);
			    t.start();
			   
				}
				
			}
				);
	
	stop.addActionListener(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event) {
				c.terminate();
				System.exit(0);
				}
				
			}
				);
	}
	
	public static void  updateCounter(int c)
	{
		counter.setText(((Integer)c).toString());
	}
	
	public static void  updateLog(String log)
	{
		
		text.removeAll();
		text.add(new JLabel("<html>"+log+"<html>"));
		text.repaint();
		text.revalidate();
		
	}
	
	public static void updateQueues(String log)
	{
		text1.removeAll();
		text1.add(new JLabel("<html>"+log+"<html>"));
		text1.repaint();
		text1.revalidate();
		
	}
}
